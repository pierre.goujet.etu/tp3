import renderGameList from './GameListView.js';
import { handleHelpFormSubmit } from './HelpView.js';
import { handleMenuLinkClick } from './Router.js';
import { Router } from './Router.js';

// Activation du lien du menu
document.querySelector('.mainMenu .gameListLink').classList.add('active');
// Affichage du titre h1
document.querySelector('.viewTitle').innerHTML = '<h1>MAGASIN</h1>';
// Modification du footer
document.querySelector('body > footer > div:nth-of-type(2)').innerHTML +=
    ' / CSS inspirée de <a href="https://store.steampowered.com/">steam</a>';
// On affiche la gameList par défaut
//document.querySelector('.gameList').classList.add('active');

// on écoute le clic sur tous les liens du menu
const menuLinks = document.querySelectorAll('.mainMenu a');
menuLinks.forEach(link => link.addEventListener('click', handleMenuLinkClick));

// rendu initial de la liste des jeux
renderGameList();
import { HelpView } from './HelpView.js';
import View from './View.js';
// on écoute la soumission du formulaire de contact
const helpView = new HelpView(document.querySelector('.viewContent .help'));

const gameListView = new View(
    document.querySelector('.viewContent > .gameList')
);
const aboutView = new View(document.querySelector('.viewContent > .about'));
const routes = [
    { path: '/', view: gameListView },
    { path: '/about', view: aboutView },
    { path: '/help', view: helpView },
];
Router.routes = routes;
Router.navigate('/about');