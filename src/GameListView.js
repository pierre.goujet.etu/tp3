/**
 * Fonction d'affichage de la liste des jeux.
 * Utilisée :
 * - au chargement de la page pour l'affichage initial
 * - et à chaque fois qu'on soumet le formulaire de recherche
 * @param {string} search chaîne de caractère recherchée dans le nom des jeux
 * @param {string} ordering ordre d'affichage des résultats
 */
const toggleSearchButton = document.querySelector(
	'.gameList .toggleSearchButton'
);
const searchForm = document.querySelector('.gameList .searchForm');
import data from './data.js';
import renderGameThumbnail from './renderGameThumbnail.js';
export default function renderGameList(search = '', ordering) {
	// calcul de la fonction de tri selon le paramètre ordering
	let sortingFunction;
	switch (ordering) {
		case '-metacritic':
			sortingFunction = (a, b) => b.metacritic - a.metacritic;
			break;
		case '-released':
			sortingFunction = (a, b) => b.released.localeCompare(a.released);
			break;
	}
	// parcours du tableau + génération du code HTML de la gameList
	let html = '';
	data
		.filter(game => game.name.toLowerCase().includes(search.toLowerCase())) // recherche
		.sort(sortingFunction) // tri
		.forEach(name => (html += renderGameThumbnail(name))); // génération du HTML
	// maj de la page HTML
	document.querySelector('.gameList .results').innerHTML = html;
}

export function toggleSearchForm(event) {
	console.log('on a cliqué sur le bouton de recherche');
	const isOpened = searchForm.getAttribute('style') !== 'display: none;';
	if (!isOpened) {
		searchForm.setAttribute('style', '');
		toggleSearchButton.classList.add('opened');
	} else {
		searchForm.setAttribute('style', 'display: none;');
		toggleSearchButton.classList.remove('opened');
	}
}

export function handleSearchFormSubmit(event) {
	event.preventDefault();
	const searchInput = searchForm.querySelector('[name=search]'),
		orderingSelect = searchForm.querySelector('[name=ordering]');
	renderGameList(searchInput.value, orderingSelect.value);
}
