export function handleMenuLinkClick(event) {
    event.preventDefault();
    const linkText = event.currentTarget.innerHTML;
    // console.log(linkText);

    /* C.3.2. Modifier le titre */
    document.querySelector('.viewTitle').innerHTML = `<h1>${linkText}</h1>`;

    /* C.3.3. Activer le lien cliqué */
    const previousMenuLink = document.querySelector('.mainMenu a.active'),
        newMenuLink = event.currentTarget;
    previousMenuLink.classList.remove('active'); // on retire la classe "active" du précédent menu
    newMenuLink.classList.add('active'); // on ajoute la classe CSS "active" sur le nouveau lien

    /* C.3.4. Afficher la bonne vue      */
    const linkHref = event.currentTarget.getAttribute('href');
    // console.log(linkHref);
    const cssClass = linkHref === '/' ? '.gameList' : linkHref.replace('/', '.');
    const previousView = document.querySelector('.viewContent .active'),
        newView = document.querySelector(`.viewContent ${cssClass}`);
    previousView.classList.remove('active');
    newView.classList.add('active');
}
import View from './View.js';
export class Router {
    static routes;
    static before;
    static navigate(path) {
        this.before = new View(document.querySelector('.mainMenu a.active'));
        this.before.hide();
        const goodPath = this.routes.find(route => route.path === path);
        goodPath.view.show();
    }
}