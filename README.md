<img src="images/readme/header.jpg">

## Objectifs
- Mettre en oeuvre le système de modules
- Savoir faire de la POO en JS
- Débugger et développer plus facilement

## Sommaire
Pour plus de clarté, les instructions du TP se trouvent dans des fichiers distincts (un fichier par sujet), procédez dans l'ordre sinon, ça fonctionnera beaucoup moins bien !

1. [A. Préparatifs](A-preparatifs.md)
2. [B. Debugger dans vscode](B-debug-vscode.md)
3. [C. Les modules](C-modules.md)
4. [D. POO](D-poo.md)
5. [E. POO avancée](E-poo-avancee.md)
